Tinyproxy
=========

[Tinyproxy](https://tinyproxy.github.io/) is a light-weight HTTP/HTTPS proxy daemon for POSIX operating systems. Designed from the ground up to be fast and yet small, it is an ideal solution for use cases such as embedded deployments where a full featured HTTP proxy is required, but the system resources for a larger proxy are unavailable.

Tinyproxy is distributed using the GNU GPL license (version 2 or above).

## To set up the proxy server
1. To customize the configuration file: tinyproxy.conf

An example:
```configuration
BindSame yes
Timeout 600
DefaultErrorFile "/usr/share/tinyproxy/default.html"
StatFile "/usr/share/tinyproxy/stats.html"
LogLevel Info
MaxClients 100
MinSpareServers 5
MaxSpareServers 20
StartServers 10
MaxRequestsPerChild 0
BasicAuth user password
DisableViaHeader Yes
```

2. To customized the docker-compose.yml file

```yaml
tinyproxy:
    image: alphacodinghub/tinyproxy
    container_name: tinyproxy
    ports:
      - "8888:8888"
    volumes:
    - $PWD/tinyproxy.conf:/etc/tinyproxy/tinyproxy.conf      
    restart: always
```

3. To start the proxy server
```bash
docker-compose up -d
```

## To test the server

Use the below python code to check if your proxy server works properly. The output the IP address of your proxy server.

file: test-proxy.py
```python
import requests

url = 'http://httpbin.org/ip'

#replace the ip address with your own server address
proxies = {'http': 'http://1.2.3.4:8888'}

#if you choose to use Basic authentation (user/password) on the proxy
#proxies = {'http': 'http://user:password@1.2.3.4:8888'}

response = requests.get(url, proxies = proxies)
print(response.text)
```

Run the test:
```bash
python test-proxy.py
```

